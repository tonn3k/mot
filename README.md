# Mot

Note: I believe mot is nearly to completed, so mot is licensed which is MIT. you can obtain a copy of this and modify it whatever without warranty, That might change later.
I'll only apply changes to this project when possible (mainly bug fix and optimization).
Edit: applied GNU license, you can obtain code of this software and modify whatever to your liking with no warranty, you need to have same license of code you received and open source it.

## Information

Mot is a simple C program reader.

## Get mot

To begin, get the source and compile it:
```
$ git clone https://gitlab.com/tonn3k/mot.git mot
$ cd mot/source/
$ gcc mot.c -o mot
```
_Pro tip: Add -O3 for optimization_

Now we're done, if you want to make mot a command
check if there is any occupied mot:
```
$ ls /bin/ | grep "mot"
```
if we dont have it, great now:
```
# sudo mv mot /bin/
```

**ALTERNATIVE: Downloading a binary file (Not recommended)**

In any reasons, you can always get the binary file in binary directory but compiling is recommended.
After you downloaded it, allow it executable:
```
$ chmod 755 mot
```
For your safety, you should check the file's sha256. You can obtain shasums in */binary/shasums*

```
$ sha256sum mot
```
_Pro tip: You can use sha1sum for bit faster check_

**If it shows any different sha than the original one, delete the file and redownload.**

If same thing occurs, you may issue this situation.

## Usage

To use Mot:
```
$ mot TXT
```
Do not be discouraged from reading wiki, it is almost simple.

## Contribute

You're welcome to contribute code but there are some requirements and limitations.

1. Code should be useful and perhaps simple.
2. Try not to use too much brackets (if if if if while for) than 3.
3. Cannot be over 1000 lines.
4. And... be independent. (no other community library)

There is also other way to contribute by reporting any issues.

## License

No need.
