/*Idea: Create a script that reads for you!*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#define maxWidth 300
#define usage "Example usage:\n$ \x1b[1;32mmot \x1b[1;33m-nl \x1b[0mmot.c\n"\
"-nl : Enable number lines\n"\
"-d [SECONDS] : Wait when continuing next line\n"\
"-f [SECONDS] : Fraction divide -d\n"\
"-l [LINE] : Read at line\n"\
"-el [LINE] : End reading at line\n"

static char filetoread[99];
static _Bool numLineEnabled=0;
static int startLine=0;
static int endLine=0;
static int delayNum=0;
static int Fraction=1;

static void delay(const int amount)
{
    const clock_t oldclock = clock();
    if (Fraction==0) Fraction=1;
    while (clock() < oldclock + (amount/Fraction)*1000000); //nano seconds to second.
}

static int read(FILE *ptr)
{
    int LineNum=0;
    char ch[maxWidth];

    while (fgets(ch, maxWidth, ptr) != NULL) {
        /*startline*/
        LineNum++;
        if (LineNum>endLine && endLine!=0)
            continue;

        if (startLine > 1) {
            startLine--;
            continue;
        }

        if (numLineEnabled)
            printf("%d. ", LineNum);

        /*print*/
        printf("%s", ch);

        /*endline*/
        if (strlen(ch)==maxWidth-1) {
            LineNum--;
            printf("\n");
        }

        if (delayNum>0)
            delay(delayNum);
    }
    return 0;
}

static void Argumentsget(char * argument[])
{
    for (int i=1; argument[i]!=NULL; i++) {
        if(strcmp(argument[i], "-nl")==0)
            numLineEnabled=1;
        if(strcmp(argument[i], "-l")==0 && argument[i+1]!=NULL)
            startLine=atoi(argument[i+1]);
        if(strcmp(argument[i], "-el")==0 && argument[i+1]!=NULL)
            endLine=atoi(argument[i+1]);
        if(strcmp(argument[i], "-d")==0 && argument[i+1]!=NULL)
            delayNum=atoi(argument[i+1]);
        if (strcmp(argument[i], "-f")==0 && argument[i+1]!=NULL)
            Fraction=atoi(argument[i+1]);
        else strcpy(filetoread, argument[i]);
    }
}

int main(int argc, char * args[])
{
    FILE * pointer;

    Argumentsget(args);

    if (filetoread[0]=='\0' || argc < 2) {
        printf(usage);
        return 1;
    }

    pointer=fopen(filetoread, "r");

    if (pointer == NULL) {
        printf("%s: the file could not be found.\n", filetoread);
        return 1;
    }

    if (read(pointer)) {
        printf("Mot experienced technical difficulties in process of reading.\n");
        return 1;
    }

    fclose(pointer);
    return 0;
}
